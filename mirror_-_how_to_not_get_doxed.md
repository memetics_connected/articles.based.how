tags: tech, opsec, mirror

# How to not get doxed

This article is sourced from [thuletide](https://thuletide.wordpress.com/2020/06/07/opsec-infosec-101/).

 - For encryption advise see [this article](https://wolfish.neocities.org/posts/articles/infosec_101_disk_encryption/) by Resurrection Europa.

 - Have a look at [privacytools.io](https://www.privacytools.io/) to understand 
how to protect your privacy online.

---

## Intro

I'm sure most people reading this blog are completely aware of this fact, but 
sometimes it's worth reminding everyone: Our enemies want us all dead. We 
aren't in a silly, harmless "culture war," at the end of which we'll simply 
shake hands with the opposition, announce that "the best man won," and then 
calmly walk away to quietly deal with the consequences. This isn't a polite 
political disagreement, but a colossal, civilization-scale conflict for the 
continued existence of our people. This conflict will never reach a peaceful 
resolution because our enemies have never, at any point, intended for this to 
end peacefully. Our enemies want us completely eradicated. They want our way 
of life gone, our history gone, our culture gone, our heroes gone, our families 
gone, our children gone. They want every memory and shred of evidence that we 
ever existed completely wiped from this planet. Their grunts would have us all 
lined up against a wall, shot, and thrown into unmarked graves this very 
second, if they had the capacity to do so. 

![racism is evil, white people are racist](https://i.imgur.com/SoHZnBJ.png)

At the very least, many people that you know in real life, perhaps even your 
friends and family, would jump at the opportunity to expose you as a "racist," 
attempt to get you fired, arrested, and generally try to ruin your life in any 
way imaginable. Never underestimate how brainwashed and anti-White the average 
person is.

Every force of society is against us. The military, police, political 
establishment, corporations, media, education institutions, banks, 
supranational organizations, "think tanks," and so on. You name it, they want 
us gone.

Take this shit seriously.


## Basic OPSEC advice

- Never use your real name, especially if you're at an irl event (trusted 
people may inadvertently spill it to someone with bad intentions). I suggest 
using a fake name in addition to your username.

- Never post pictures of yourself.

- Don't talk about what events you've gone to.

- Never post pictures of/from your house, car, or pets.

- Never use the same username twice.

- Use usernames that are not super unique and/or are difficult to get info on 
from google.

- Don't ever link to another one of your accounts using a different username.

- Remove info you put on old websites you've forgotten about.

- Frequently change identifiers that allow you to do so (twitter @, etc.).

- Don't use the same unique avatar on multiple accounts. Reverse image 
searching unique avatars is just as precise as searching for unique usernames.

- Don't post shit about your birthday; better yet, celebrate a fake birthday.

- Be vague about your age and occupational, scholastic, and familial situations.

- Stop asking/responding to "who here is a nevadan groyper?" etc. in live chats. 
People doing that get muted for a reason.

- Lie about yourself. Giving fake nuggets which could be used for pursuing a 
false dox is an extremely effective way of throwing ppl off your trail.

- Avoid normie social media, or at the very least DO NOT corroborate what you 
put on there with what you put on your anon accounts.

- Don't follow IRL friends on your anon accounts.

- If you have some undying urge to talk about familial issues (grandma is sick, 
etc.) with anons on twitter, only do so PRIVATELY.

- Don't frequently interact with tweets from an overly-specific region (i.e. if 
you're from Buffalo, NY don't constantly be liking shit from Buffalo/Erie 
County/upstate NY).

- If you're staying off normie social media, also request that close family 
members don't post pictures of/references to you on their social media.

- If you are a sperg that is unsettlingly obsessed with guns/knives, don't talk 
about it. You're not impressing anyone, and it draws ire.

- Use a DIFFERENT ANON email/phone number/password combo for EACH website 
you're on. Use http://autofications.com for phone numbers.

- Email http://archive.org to request that your accts not be included on the 
wayback machine (they'll oblige so long as you can prove you're the owner).

- Contact public background check websites to request that your/your family's 
information is taken offline if you're able to find it.

- Use a VPN if possible; if you're being threatened by someone with your public 
IPv4, don't acquiesce. This only exposes your ISP's location.

- Download & use Tor browser, especially if you are in a more totalitarian 
country, Germany or Britain, for example.

*Edit:* Tor has alternatives now: I2P, Freenet, Zeronet, GNUnet.

Other tips:

- Go private and try your absolute best to dox yourself with what you have 
publicly available.

- Don't EVER reveal even a peek of your power level irl.

- Don't get cocky and assume people won't try to ruin your life because you're 
a nobody.

- ALWAYS MAINTAIN PLAUSIBLE DENIABILITY.

OK, stay safe. Share this article with a friend to stop them doing anything 
stupid and compromising their OPSEC.

![Sam Hyde](https://i.imgur.com/l10fOXW.png)