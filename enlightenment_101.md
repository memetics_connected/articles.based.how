tags: spiritualism, self-improvement


# Enlightenment 101

There is a certain **superior mode of being** available to most human beings.

It is obtained through exercise of the mind, of the body, and of the will or
spirit.

This article aims at giving you some basic exercises that can open a path 
upwards.

> Moral virtues, like crafts, are acquired by practice and habituation.

Aristotle.


## Meditation exercises.

My conclusions are at the end of this section, you should try to reach your own  
conclusion before reading them.


### General meditation advice

It isn't about **not** thinking. Just focus on your breathing.

Comfort and lack of distractions is important.

Sitting or crossing your legs are both fine. Even laying down, just don't fall
asleep.

Before starting the exercise, meditate by focusing on your breathing for about
5 to 10 minutes. You need a relaxed state of mind.

A relaxed body helps too, so get rid of tension. Just don't fall asleep.


### 1. Observe your thoughts

> We all have the mind of a monkey.

This mind is easily distracted, we are bombarded by thoughts.

After focusing on your breathing for 5 minutes or so, try to visualize your 
thoughts. 

Imagine they are pieces of paper, which come and go with the wind.

Or boats, holding your thoughts. They come, stay around a bit, and then leave.

- Visualize every thought as it comes and goes.


### 2. Using the Via Negativa to define yourself

What is me, what is I, what is the soul? 

These are complex questions, so instead of answering them directly, take the
negative approach and try to answer what they are not.

- Sit down, relax, close your eyes, breathe, etc.

- Once you are in the right state of mind, ask yourself what aspects of your
most superficial personality traits **are not** yourself.

You may start with the posters in your room, they aren't really you, they are 
only a trait, a hobby, something you like.

> Would you still be you without it?

The first time you do this, try to peel back the most superficial layer of
shallow **personality**, your likes, dislikes, and hobbies.


---


#### Conclusion to "Observe your thoughts"

There are a couple of things to be learned here.

- You realize that you as an "ego" and "your thoughts" are distinct, or they 
can be. 

- You can energize certain thoughts or let them go. 

- You can distracted yourself if your pattern of thoughts displeases you.

- You can distract your monkey brain by focusing on your breathing, but you 
will inevitably falter and stop.


#### Conclusion to "Defining the self by the Via Negativa"

You should have a more grounded and precise view of what you are. You may never
get more than a glimpse at what the center of the onion is like.

But at the very least, you should know the difference between shallow things
and deep things when it comes to what defines **you**.

Perhaps you have also realized the difference between **me**, and the **I**.


#### Conclusion to both

There are several components to *you*. Learning about them, naming them, 
categorizing them, gives you wisdom over yourself.

This is the training of the soul.


#### Extra: Breathing well

Breathe from your diaphragm, not your rib-cage.

Most people normally breathe by expanding their rib-cage outwards and inwards.

Musicians don't do this, instead they fill up their lungs by using their 
diaphragm. So do monks, yogis, etc.

Breathe in through your nose.

- If your nose is congested, try to open your jaw, while holding your mouth closed
as far as it will go. Move your jaw slightly to the left and right, this will
clear your sinuses.


---


## Training your willpower

This one is simple.

Get down on the floor, do push-ups until it hurts and you can't do it anymore.

Then keep doing it, even if your arms tremble.

Do it until there is no strength left in your arms and you cannot lift 
yourself.

Get up and do some squats. Do it until your legs are on fire.

Then keep doing it. That's it.

The point is simple:
Practice the principle that your body does what **you** want, not what **it** 
wants.

Your animal body wants to fuck/masturbate, eat, sleep, and play videogames all
day every day. **Teach it who is boss** or otherwise it will be the boss of you.


### How willpower works

Through the meditation exercises, we should come to an understanding of your
conscious thoughts and that **you**, and **your thoughts** are not the same.

Let's go further below then: Your body.

The above is your mind, the below is your body.

To go lower, is to be dominated by animal instincts and to go higher is to
be dominated by your own highest self, or perhaps God.

But there's another "law" which [Hermeticism](https://cdn1.booksdl.org/ads.php?md5=2BBEBB1E5D5620F47CE3B2FA59346D02)
teaches.

The hermetist can use the law of the above to bend that which is below.

Above is your mind, below is your body.

> Mind over body.

This practice is critical to build character or willpower. Your willpower is as
much a fuel as it is a habit. You must actually practice to build up willpower.

The ancients had names for this, I suspect Qi may be a similar concept but
this is irrelevant. Your willpower increasing will be a noticeable and tangible
force you can add to your arsenal.


#### Conclusion

Your willpower or *character* is the simplest to train.

A rancher has a high degree of willpower by natural circumstance.

But a programmer or someone who lives a comfortable life does not.

It is also the most difficult, as training it requires trespassing your limits 
and comfort zone consciously. Just try not to die of hypothermia after swimming
in ice-cold waters.


---


## Fasting

>For we fast for three purposes: to restrain the desires of the flesh; to 
raise the mind to contemplate sublime things; to make satisfaction for our 
sins. These are good and noble things, and so fasting is virtuous.

St. Thomas Aquinas.

Focus on the purpose of fasting; not on the process. With fasting, we can 
dominate our hunger.

There are more and less extreme methods about this.

You should also not fast until it weakens you or puts you out, it is a 
challenge imposed on your sense of control, not your body.


### A small note on eating

You should eat until you aren't hungry, and especially during lunch you should
pay close attention that you don't over-eat until you are full.

Eating too much for dinner will fatten you up.

You should eventually be able to tell when you're done eating, and notice that
your body isn't asking you for more to sate your hunger, but to satisfy your
cravings: at this point, you are gorging yourself on food.


### The gentle method: Don't have dinner

Eat two meals daily: A good hearty breakfast and lunch.

- break-fast is called so because it is the breaking of the fasting.

Do not have dinner.

You can either do this for a couple of weeks, or forever. This is a gentle 
method and has health benefits including an increase in white cells.


### The Christian method: Lent, Ash Wednesday and Good Friday

Do not eat anything for Ash Wednesday and Good Friday.

Additionally, during the period of Lent, it is customary to only eat one meal 
and two snacks, which don't make up a meal when put together.

Fasting is considered as: Eating three times a day, once a full meal, and the 
two remaining snacks should be small, and not constitute a full meal when put
together. But this is considered as the minimum.


#### Extra: Eating once a day for great health!

In several studies, it has been shown that rats who eat once a day and sleep
in the cold have a more active immune system than rats who eat several times a
day and drink in the cold.


---


## Fitness

> No man has the right to be an amateur in the matter of physical training. It 
is a shame for a man to grow old without seeing the beauty and strength of 
which his body is capable.

To the Greeks, fitness wasn't a simple mundane thing to do to buff up.

It was an ethical matter, related to statesmanship, and virtue.

In order to have a healthy mind, we must have a healthy body.

After all, your mind is built on top of the edifice that is your body.

Fitness is a requisite to enlightenment.


## Dieting

> We are what we eat.

Eat trash and that's what you will be. Processed foods will kill your soul.

Coffee will have negative effects on your path upwards. As will sugary drinks.

I will not advocate for veganism, but do remove all processed and artificial
foods from your diet. Without a doubt you will live better.

