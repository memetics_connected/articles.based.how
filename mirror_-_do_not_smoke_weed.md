tags: mirror, drugs, addiction

---

The original source for this article is a [twitter](https://twitter.com/owenbroadcast/status/1090086693556928513)
[thread](https://threadreaderapp.com/thread/1090086693556928513.html)
by [Owen Cyclops](https://owencyclops.com/).

Modifications have been made to remove twitter-isms, and the order of tweets 
has been changed.

Owen has consumed weed for longer than 10 years, and this article is his story
on how it affected him and will likely affect you.

> Do not eat the lotuses.

---


# Do not smoke weed

> Basically just wanna riff on the role weed has played in my life and why I'm 
really sick of it.


## How Owen started using weed

So, I actually didn't start smoking weed until after high school. My parents 
smoke a ton of weed, and this was enough to turn me off from it until then. It 
was extremely embarrassing having them show up to pick me and friends up high 
or something, I had an intrinsic antipathy to it.

However when I went to college I got really into it, probably for a variety of 
reasons I'll explore here. One thing I noticed instantly was that it put a 
huge damper on my naturally buoyant and happy go lucky personality. The effect 
was immediate and manifested in many ways.


## Weed is addictive

> You become dependent on it to have a good time.

As I got older and had basically been smoking all the time for years, I found 
that I really needed weed to have fun. people that smoke a ton of weed that act 
like this effect never manifests or that you "cant be addicted to weed" are 
completely totally lying 100%.

I smoked weed 24/7 for like ten years and basically all my friends did as well 
so its not like I have no idea what I'm talking about. I know exactly what it's 
like. You become dependent on it to have a good time. You feel like you don't 
*need it* because its always around.

You develop a circle of friends who basically only hangout via smoking weed. 
Its the only way you relate to each other. Of course you have other real 
friends (hopefully...) but you also have friends where you KNOW if you couldn't 
smoke with them it would just be awkward, obviously.

And in this way you slowly start to lean on weed where you tell yourself you 
don't *need* it but you know if you had something planned and you were going 
to smoke and suddenly the weed left the picture, the activity would be less 
fun for you, maybe you wouldn't even do it.


### Things stop being enjoyable without weed

I had a friend who set out to hike the entire Appalachian trail (Georgia to 
Maine). Total stoner. Brought a ton of weed. Got like halfway through and ran 
out. Called me and others trying to get us to mail him some. Couldn't get it. 
So he bailed on the trail and came home. 

Now when you ask him why he got off:

> Oh well it stopped being fun.

> I just kind of wasn't sure why I was doing it anymore

> Well I got what I needed from it

A ton of abstract reasons, **obviously** it stopped being cool because he 
couldn't smoke weed anymore and couldn't go without it.

This is a perfect example of what its like. Not smoking becomes a thing and
not being high starts to have this *tinge* to it. if you took the average 
stoner to the most beautiful Mediterranean island in the world the cloud of 
not being able to smoke would hang over them and dampen it.

I lived this. I traveled with people, often to beautiful places. We'd get there 
and the stoners, including me, would set out to find weed. *Oh haha we don't 
need it or anything its not like we need it to have fun its just obviously our 
first priority to find some.* pathetic.

I even openly discussed this effect with people. I did ayahuasca a few times 
and one of the effects of that is it kind of resets you (not recommending it, 
just saying). People often say something like "I feel like myself for the first 
time in a long time" or something like that.

When I talked to stoners about it, I'd say, "yeah, it makes me feel like I 
don't need to smoke 24/7, it takes the tinge away from not being high". 
Everyone I ever told this to knew exactly what I was talking about. No one 
ever was clueless. They know.


### The death of personality

> I enjoy being fun with people. When I started using weed I could feel this 
aspect of me drain away.

> I think most people that smoke a lot learn to write off these effects and 
ignore them, but they pile up, and slowly begin to change your personality...

I'm not "goofy", I'm into many serious topics, but IRL (at the risk of 
sounding pretentious as hell) I think I have kind of a wise fool vibe going on 
where I do enjoy joking around, making people laugh. I enjoy being fun with 
people, basically.

When I started using weed regularly I could literally feel this aspect of me 
drain away. I became more self-conscious in a way, and combined with the 
naturally sedative effects of weed, I stopped joking around with certain 
people in the way that I previously had.

This seems like a dumb example, but I used to dance with my friend in his car 
when we'd listen to music that we liked. Like moshing in the car, or do other 
stupid shit. This totally stopped when we started smoking. This isn't 
significant really but it indicates the effect I'm describing.

I think most people that smoke a lot learn to write off these effects and 
ignore them, but they pile up, and slowly begin to change your personality 
in ways that you don't think are significant, but obviously are. You ignore 
these small changes, or attribute them to something else.


### Pull the string, and you loose out on life

So, I have some atypical ideas about time and how things appeal to or cling to 
the spirit (this is relevant, not a high detour, lol). Basically I think there 
is a reason certain things make a big impression on the mind and sometimes 
those reasons are some future event or theme.

When I was super young, must have been like first or second grade, some 
teacher or caregiver showed us this video of different stories or fables and 
one of them stuck out so clearly in my mind. Like it totally blew my mind wide 
open and I always remembered it clear as day.

Even before I figured it out I thought this was weird. I mean I was so young I 
don't even remember where I was or who I was with or anything, but I could 
draw parts of this short video for you, it was instantly burned into my mind 
like a hot iron. now I understand why.

The story was, there was this guy who was always bored and always wanted to be 
other places. He was always impatient and always wanted to know how things 
would turn out, he could never wait for anything, he was never happy where he 
was.

The guy was young, like a teenager. So one day he meets this magical person 
who gives him a magic ball of string. In the video it looked like a glowing 
white ball of yarn. He tells him, when you pull this string, you can skip 
parts of your life.

Pulling the string basically fast forwards his life slightly. So if hes 
waiting for something, he can pull the string and jump forward a few moments. 
Or if he wants to know how an event can play out, he can pull the string and 
fast forward and see how things end up.

So he takes it, and starts doing that. He's waiting for the bus, he pulls the 
string a little and the bus is there. He has to take a long drive, he pulls 
the string and hes at the end of the journey. He wants to know how a fight his 
friends are having ends, he pulls the string.

Eventually he starts pulling the string randomly, whenever he feels like it. 
He's bored, he pulls the string. He's talking to someone, he pulls the string. 
He just starts doing it just to see what will happen. He pulls it hard, He's 
in college, at his wedding, he just keeps doing it.

Eventually he's an old man and he's like "woah what the fuck. I'm old now... 
and I've like, completely fast forwarded through my whole life", and he has a 
panic attack. The person who gave him the string returns and says, "yeah, 
you're a fucking idiot, you fucked up", essentially.

He gives him another ball of string that will send him all the way back to the 
beginning, and he pulls it. Of course he learns his lesson and gets to relive 
his life appreciating all the moments that he just saw as annoying obstacles.

That's exactly what weed is. You can use it to coast through parts of your 
life. I used to say smoking weed is like you're biking, and when you light up 
it's like you start coasting downhill. It takes no effort and suddenly, when 
you were just peddling, you can just ride with no effort.

You can just breeze through parts of your life. ,y cousin once said something 
like "yeah if I have to pick through a dumpster of trash at work of course I'm 
gonna smoke beforehand". It can make bad things tolerable. But it does this by 
numbing you, by emptying you out. *Pull the string*.

Everyone who has smoked a ton of weed knows the feeling of getting high as 
fuck and going somewhere or doing something and afterwards you're like "did I 
even do that? man I feel like I wasn't even there". Well, you weren't. People 
live huge chunks of their lives this way. I did.

I truly feel that the video I saw burned itself into my mind because that was 
going to be one of the main struggles of my life. My whole family abuses 
substances. The second I started using them I did also. I'm that kid that was 
pulling the string, I just got lucky that I stopped.

People say its harmless but the reality is that *most* of the time the fact 
that it doesn't cause overt harm is the most dangerous part. You never hit 
rock bottom with weed, you just keep tapping into this void and you don't 
realize you're becoming this boring as hell empty person.

>But my one friend smokes and has a hot model girlfriend and a mansion.

>Bro I smoke and I'm totally fine

Okay whatever. If you have stoner friends you know EXACTLY what I'm talking 
about and if you say you don't you're just kidding yourself full stop.

Literally everyone that smokes weed a lot has that 1 friend where you see him 
"hey man whats going on", "nothing bro just chilling": does literally nothing 
with his life, is the same **exact** person he was when he started smoking, 
everyone knows it stunted him, it's painfully obvious.

Part of the reason I'm going so in depth into this (aside from getting some 
stuff off my chest hey it's my account whatever) is that I think people who 
don't do a lot of drugs or aren't around them think this stuff is like a meme 
or exaggerated. It is not; Stoners lie about this stuff.

I enjoy ridiculous boomer anti-drug aesthetics as much as the next guy. Most of 
the DARE ads are ridiculous. However somehow someone snuck 
[this one](https://video.twimg.com/ext_tw_video/1090076013512589312/pu/vid/240x180/NC87833SJDYMiM9e.mp4?tag=6)
I'm gonna post through and its so spot on that the first time I saw it it made 
me deeply uncomfortable. this person knew stoners. If you smoke weed a ton you 
will absolutely know is stone cold accurate as hell. You probably know this 
person, hopefully its not you.


## The Lotus eaters

> It's like this land that I step into and I'm just blasted with weed smoke and 
drink and I become one of them.

So... why do I care. well I care because essentially everyone I grew up around, 
and my parents, are *lotus eaters*. whats that? lets find out...

Basically (I know you just read it but roll with me here) Odysseus finds this 
land populated by these people that just eat lotuses all day. The lotuses grow 
freely and induce a blissful state. When his men go ashore and try them, they 
want to stay there and become lotus eaters too.

They just wanna stay there and be drugged out all the time. They forget about 
home and what's important to them and just want to chill and eat these fucking 
lotuses. He has to literally drag them back on the boat. In some versions I 
heard some men say *fuck it*. They stay, becoming lotus eaters.


### Traveling back to Lotus Island

That's exactly how I feel about my family (sorry if your reading this at some 
point in the future I gotta be real with the people though) and all my old 
friends where I'm from. It's like this land that I step into and I'm just 
blasted with weed smoke and drink and I become one of them.

This happened to me not too long ago. I live clean where I'm at now with my
girlfriend. I cant take total credit for this, I mean I moved and couldn't get 
weed, wasn't really sure if I wanted to bring that vibe to my new place but, 
being addicted, I probably would have if I had found it around.

I went back home for about a month and the second I got there, bam, I was 
right back in it. It was like I had made no progress whatsoever. It's like I 
feel the call to be a *lotus eater* deep in my blood. This makes sense as it's 
my mythological and literal origin.

I was just blasting on that string man, pulling that shit so hard, like I 
never even gave it up. And after my month there I was like, wait, what just 
happened man. My plane ride home was like Odysseus dragging me back to the 
boat, and I felt myself become a real human again.


### Escaping Lotus Island

This made me reflect on my life, and growing up, and my parents, and how I'm 
trying to eventually have a family with my girlfriend, and about how 
normalized all this was for me. Once growing up, a friend told me he had 
never seen his parents drunk before. This completely blew my mind.

He was not a lotus eater, obviously. And I got to thinking, you know, fuck 
this. I don't have to pass this garbage lifestyle onto my kids, I don't have 
to passively accept all this shit. I don't have to slavishly adhere to this 
lifestyle that was placed onto me like some sick cloak.

So I decided I'm not going to be that person anymore. thinking about all my 
stoner friends stuff just makes me sad now. I cant drag them onto the boat 
unfortunately but at least I can show them the option is there, or at least 
I can set off and do my own thing.


#### Stoner friends are fake

Oh, here's a bonus, essentially all of the friends you acquire via using 
substances turn out to be fake friends. I know, shocking right. This should 
be obvious but when it actually happens and people you've known for like +8 
years just fall away, you're still surprised.

I'm talking to **YOU**, *guy in his early 20s reading this right now*.

Spoiler alert: they're not your real friends. The odds that your just being a 
ghost in your own life via weed is extremely high. That's why you feel like no 
one knows you. You're not there. There's no one to know. Think about it.


## False mystic element of drugs

Some people (like my old self) feel like weed has some *third eye* component, 
that it kind of accesses a more "spiritual" way of being or seeing things.

Probably sounds dumb but some people will know exactly what I'm talking about 
so now I'm talking to them.

This is misplaced theological and spiritual energy. You're feeling a call to 
elevate your mind and change your spiritual state and you're giving yourself 
the illusion of doing something.

Basically, when you smoke weed, you are rolling up a fat blunt of what the 
orthodox call [prelest](https://orthodoxwiki.org/Prelest). 
This is a great word, in my limited understanding it is basically spiritual 
delusion. It's like being hopped up on spiritual delusion that aggrandizes 
you in some way. That's what you're feeling.

Spoiler alert: psychedelics do this also. It's like fake theological energy. 
You feel like weed is giving you these deep thoughts and insight but when you 
look around, all the other people smoking weed obviously don't have some 
special insight or anything like that.

Well that's because its not. It's a total illusion. All the deep thought 
patterns you feel like weed gives you are illusory. That's why total idiots 
from any subculture and place can smoke a ton of weed and stay idiots. If it 
gave you deep thoughts, they would be having them. They're not.

> Yeah weed makes you sit around and enjoy feeling your beard grow.

I thought this was a weird thing to say but it kind of ruined smoking for me 
because I realized I thought I was having deep ideas but really I was just 
clearing my brain and doing nothing.

Psychedelics aren't 100% prelest... Sometimes the first time people take them 
it turns them on to religion or something, alright, I seen that, but prolonged 
use of them is essentially always prelest.


## Weed lowers your inhibitions and sense of disgust

The other thing is that it lowers your ability to resist other things. The 
stereotype is someone who is high will just like, eat a huge bag of chips or 
dive hard into a bowl of candy. If you smoke a lot you know its a lot harder 
to resist these things while high.

But other temptations are like that also. A huge light went off in my head when 
I realized that I only watched porn after smoking. I noticed this last time I 
was back in lotus town. When I wasn't high, the idea of watching porn was 
disgusting and I never did it.

I couldn't get over the hurdle of actually going to a porn site, navigating 
through all that filth, turning myself on, getting into it, the whole process 
seemed ridiculous.

But then if I smoked, it was so easy. It was so easy that it was almost 
difficult not to. the hurdle was gone.

Just being overly real here as other people will definitely know what I'm 
talking about here. This effect peaks when you're high as fuck but you slowly 
**retrain** your brain to break down these hurdles and it becomes easier and 
easier to give into temptations, high or not.

This is something else I noticed in me and in others. Sometimes hanging with 
stoners (they're always high but whether we just smoked or not) there's this 
mindset of indulging in doing things you "wouldn't normally do", but then in 
reality you start doing them all the time. *Less hurdle*.

This is kind of a subtle abstract process I cant really explain but some 
people will know what I mean. It's this mindset of indulging in breaking your 
own rules, eating excessive foods or binge watching stuff, or any temptation 
really. Weed lifestyle makes it easier.

You get used to coasting and your brain starts to enjoy that mindset of "oh, 
right now isn't one of those times I have to play by the rules, I can eat this, 
do this, watch this".

Weed is hooked into all many other seemingly unrelated aspects of your life, 
it's really one big picture.


#### Weed smokers habitually drive while high

Oh also if you're isolated from stoners and drug culture, they all smoke while 
driving. Can't think of one person I know who smokes weed that I haven't seen
smoke weed **while** driving:

[Habitual marijuana use strongly associated with car crash injury](https://www.ncbi.nlm.nih.gov/pubmed/15847617)

Guarantee if I called any of those people right now they'd tell me it doesn't 
affect their driving.


## Weed becomes a "part" of you

A shaman says when you do a substance you're allying yourself with that 
substance. This is true.

I'm not an animist but its a good metaphor.

They would say that the spirit of that substance gets imparted to you and 
becomes part of your life. Obviously a desert shaman isn't on peyote all the 
time but hes allied with it and the peyote spirit heavily informs his life.

> Do not eat the lotuses.