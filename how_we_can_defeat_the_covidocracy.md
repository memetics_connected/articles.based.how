tags: covid, society, strategy


# How to defeat the Covidocracy

Global authoritarianism is knocking on the door.

The plan is to create two castes, an elite of neo-feudal overlords who control
your thoughts through propaganda, and a peasant class born of broken families,
and with a piss poor education (remote learning doesn't work).

This plan has 6-7 years to be implemented, after this, it is assumed by the 
enemy that resistance will mount to a point where it becomes unfeasible.

Read all about it from Klaus Schwab's [The Great Reset](https://www.amazon.com/COVID-19-Great-Reset-Klaus-Schwab/dp/2940631123/)
Or better yet: [pirate it](https://libgen.rs/book/index.php?md5=818DC49A2DE2256A0E019CDF93AF984C)


## In order to defeat tyranny, we must:

- Strengthen our communities. Create traditional safety nets. Start with your 
family.

- Homeschool, you're the only person left who can educate your children.

- Create parallel institutions, avoid state, bank, and corporate institutions.

- Not get the injection. This both delays them and protects you.

- Strengthen yourself.

---

### Strengthen our communities

This is essential.

*Any* responsibility relegated to the state, such as education, water 
processing, or the media (propaganda machine) *can* and **is** being used 
against you in this war.

This is not a culture war. It is a war for the soul of humanity, and very 
likely the last one, as they have now struck our children. Never forgive them.

Aim at having a minority of about 10% of people in your village in contact with
each other, and capable of helping each other. Churches facilitate this.

Organize a cook-out, get to meet everyone. Don't be a neet. Save humanity.


#### Family, then community

Your family is your foremost asset in the war, they love you and so do you, so
get them involved in this, they already know something is wrong, it is your job 
to guide them out of the darkness. 

Your community is likely ravaged by the social weapon known as covid 
restrictions. Some churches combat this. The social communication war front can 
be expanded to the internet:

Places such as Gab offer a "diaspora" of right-wingers who you can and should
connect with, find someone close to your area and bring them a cake, works 
every time.

There are always forums like this, search for "antisemitism forum", "racist 
forum" or similar and you will find the places with the best people: the media 
is their weapon, and they will use any and all slander to discourage you from 
connecting with others.


### Homeschooling

Nobody has any interest in teaching your children about strategy, political
economics, philosophy, or history.

You are the last man, truly the last generation that went to school. Regardless
of whether the plan is implemented or not, at the current time, education is
entirely dead.

You **MUST** revive the education of the youth or prepare to do so, or flatten
the road as much as you can for others to teach.


#### Public education, especially in the US, is trash.

In Europe, it creates an emasculated moron incapable of illegal action, fighting
or any manly act of war.

In the US, it creates an ignorant fool, still somewhat manly, but utterly 
lacking in philosophy on how to apply it.

In both, it makes men weak and women whores.


### Create parallel institutions

Try to post anything on *their* platforms that disagrees with the narrative...

Twitter, Facebook, Instagram, Youtube, and many other places have implemented
methods to undermine you; to delegitimize you.

This applies to every other facet of human (social or economic) activity. They 
want to immobilize you and corner you, make you depend on them.

**Any** act that makes you more independent of the system and closer to your
community, such as raising chickens and selling the eggs, is a net victory to
this war.

A local money-lending union can save your community from being torn apart by
the greed of the bankers.

Local business, and consciousness of the local population can resist corporate
takeover, but irresponsibility from the locals can lead to corporate overtake.


##### Reko rings or farmers markets.

Contact the farmers. Form a **Reko Ring**, an association where farmers put up 
ads for the product they sell, and get contacted by buyers. Meet up once a week
and have the farmers bring their food.

Or a farmers market, this is inferior in that the farmer is forced to sit 
around all day with maybe unsold merchandise,


##### Water, Food and Electricity as strategic resources.

The first civilizational necessity is water; it cannot be shut off granularity, 
it can only be denied to vast populations. This would constitute a Pyrrhic 
victory if used as a weapon.

The second is food; it can be poisoned, it already is in the US. How many 
additives do you eat every day? How much of the dietary advice Americans follow
is false? (eggs do nto raise cholesterol, vegetable oils are worse than animal 
fats).

The third is electricity; Same problem as with water. It can be made more 
expensive or taxed through ecology laws, forcing you to buy whatever the elite 
considers good. Good tends to mean more controlled, restrained, less free.

For example, electric cars: They are utterly dependant on the electric grid.
Gasoline cars can have their fuel stored and run great distances, horses are 
even better but whoever the hell rides a horse to the office?

I plan to.


### Strengthen yourself

This also involves removing weakness. What you do not control, controls you.
In this vein, stop using your smartphone for a week, remove it completely from
your life if you can. Stop watching pornography. Lift weights like a maniac. Go
on miles long treks with a heavy backpack. Pray three times a day if you're
Christian, etc.

Now or never. There will be no second chances, if you fail, this is how the 
humanity you knew and loved will die, and likely never recover.

Always remember however, that to ascend as a man is admirable, but to ascend
as a people is glory without equal.


---

## Neat things you can do:

Some of these aren't directly related to the war against globocovid, but they 
are neat nonetheless, and will raise your *morale*. Treat yourself as a unit
of infantry and you will see great results.

-  If you have our own farm and produce compost, you can 
[harvest methane](https://yewtu.be/watch?v=u84s_ObQn-0).

- Consider *guerrilla* growing bees 
[in plastic bottles](https://yewtu.be/watch?v=9ItlOFLTUAs).

- Quails can be grown [in an apartment](https://yewtu.be/watch?v=2kajwyWlD34)
(ha).

- Make traditional survival foods such as 
[pemmican](https://yewtu.be/watch?v=HQZj1-lSilw) or
[hard tack](https://yewtu.be/watch?v=FyjcJUGuFVg).

- Dry your own food with a [solar dryer](https://yewtu.be/watch?v=1W6lCJAT9zo).
You can dry anything, seasoned tomato slices, mushrooms, meat, spinach, tea, etc.

- Or preserve them via [canning](https://yewtu.be/watch?v=5tUK3zf_56Y)! 
This is a very fun activity to do involve the whole family.

- [Make a kayak and go sailing](https://yewtu.be/watch?v=9MFN2tXnd0Y).

- Grow lettuce via the [kratky method](https://yewtu.be/watch?v=5z0NaYpVHMs).

- [Plant a herb garden](https://yewtu.be/watch?v=qbYbI-uGEyg) and cook with them.

- [Make a PVC bow](https://yewtu.be/watch?v=NdyJB0O3fSQ) and learn archery.

- Learn how [post-modern](https://yewtu.be/watch?v=ihKSHT7gIBY) 
[warfare](https://yewtu.be/watch?v=N7Yr1hYcrp0)
works.

- Print out a *it's okay to be white* poster and put it up around your local
university.