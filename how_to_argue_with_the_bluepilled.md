tags: politics, leftism


# How to argue with the bluepilled

This document will give some insight on how to argue, and when not to waste your
time, out of the author's own experience.

Some people lean to the left, but only superficially, others to the right but
follow the gospel of Ban Shapiro or Jordan Peterson. Regardless, they are your
targets.


## The short version

1. Everyone who can tell that the world sucks is a target. 

2. You will listen, and reveal the truth about topics *they* bring up.

3. Learn to spot talking points and what works best against them.

4. *Do not get on their nerves*. 

5. Push only that which they can tolerate.

6. Do not waste your time with zealots.


---


## The long version


### 1. Everyone who can tell that the world sucks is a target.

Maybe it's about how the current Covid mania and vaccine mandates stink or have
an air of authoritarianism about them.
Maybe they lost their job or cannot get up the corporate ladder because they're 
white. 
Maybe they're frustrated because art and architecture has no beauty to it 
anymore.
Maybe they're joking about how Netflix or the BBC has made a white historical
figure into a black actor.


### 2. You will listen, and reveal the truth about topics *they* bring up.

For example, they may bring up how *a friend lost their house*, for being unable 
to pay a *mortgage*, explain usury, how it has been practiced by banks, and how
it is done so by design and as a predatory attack on the people. Do not allow
them to pivot into how the government should regulate it, by explaining insider
trading; the government is on the team of the banks and corporations.

You may think this is a defensive war you're waging, not so.

You are waiting until intel on actionable targets shows up, maybe prodding and
bringing up topics, to see if they react. When they have shown an interest in 
something, you know they will listen. People only listen to topics they have
an interest in, otherwise they act as if they were listening out of niceness.

Then, when a target is confirmed, fire appropriate ordinance.

You can prod for targets, but you cannot fire artillery at an uninhabited island 
and expect to win the war can you?



### 3. Learn to spot particular talking points and what works best against them.

**This section is at the end of the document.**


### 4. *Do not get on their nerves*. 

Normies get easily frustrated, most of what they know is false and it can be 
scary to have your worldview be proved false, this already happened because of
Covid signaling the end of the western (((democratic))) state model, so always
remember that they are *babies* crying out, **not enemies** striking at you.

Having your beliefs challenged is difficult, you have to be patient and roll 
back sometimes: let them tell their stories and add to the conversation, even 
if what they say isn't very meaningful. 
In fact, do everything you can to make them feel like an equal to you, like they
are adding to the conversation meaningfully.
But never forget you aren't an equal but a teacher.

There are other requirements to take into account, such as not grating on their
nerves, looking attractive or desirable (well-groomed, fit, married), and
speaking without any signals of lack of confidence such as stuttering.

This is because human beings, as animals, **first** judge by instinct/biology 
and only then by rationality, sometimes hours after a conversation when they 
shower of before going to sleep.


#### Do not attack or accuse, even if it is true.

People will become defensive if they feel attacked.

If you find one of their positions disgusting, hide it for now.
Have them talk while you clam down.
If you accuse or attack someone, even if it is the truth, or tell them what 
they think, they will become defensive and you will get nothing done.

Remember, this is your fight to fight, they lost it. Not surprising taking into
account how many resources were invested into the creation of a perfect cog.


### 5. Push only that which they can tolerate.

This is an extension of the above.

Some bluepilled normie may bring up something about race, and you will be 
tempted to drop redpills on race as if they were on sale.

This is easy because we already understand race, but they are scared by the 
truth, in the same way a woman may be scared of admitting they were wrong.

So, learn what someone will tolerate, as they may have lines in the sand, where
the normie will feel legitimized in dismissing you for having stated a 
"completely unacceptable" position.

These include claiming blacks are animals, the holocaust was entirely a 
fabrication or that women shouldn't have rights.


### 6. Do not waste your time with zealots.

On the other hand, some people aren't interested at all in hearing what you
have to say and are more interested in spouting ideological propaganda. 
Learn to recognize them and ignore them, or humiliate them if there's a camera 
rolling. Publicly making fun of leftists is a great strategy.

There are two ways of doing this.

You can go above their level and humiliate them, watch "leftist humiliated with
facts and logic compilations for this".

Or, you can go below, and simply, without hesitation, laugh in their face in the
most honest and direct way you can bring yourself to. Be obnoxious, direct, 
crass. Do not insult them, simply treat them as the clowns that they are, the 
very notion that someone is a feminist should be the funniest and most absurd
thing to you. This is because it is, we're just too nice and don't laugh enough
in their faces.

Human beings have no psychological defense against being openly mocked in the
worst way possible (being laughed at). That is, unless they are autistic or 
psychopathic, or strong of character -> leftists aren't strong.

	
---

## Leftist talking points
Or...

*3. Learn to spot talking points and what works best against them.*

**Leftism isn't a philosophy but a set of learned slogans and talking points 
approved by the oligarchy.**


- The state is the reason there isn't any criminals running around knifing you.

	- Criminality correlates to presence of certain races, blacks for robberies
	and Mexicans for rape, Arabs for knife attacks.
	
	- Criminality also correlates to IQ.
	

- IQ is invalid/racist etc.
	
	- IQ is the strongest predictor or labor success at .5 correlation.
	
	- Show them [this test](https://gotest.pk/forces/non-verbal-test/intelligence-1-online-solved-answer-explanation/)
	
	- Yes, IQ is racial.
	

- Fact-checkers said this was false.

	- Media is propaganda, Fact checkers are counter-propaganda.
	
	- 6 corporations run the entire media, including fact-checkers.


- Whites are over-represented as CEO's

	- It's jews, not whites. (Jews aren't white)


- Immigration is good for a country!
	
	- I agree: Israel should open their borders and accept refugees.
	
	- Provide historical examples. (There are none).


- US is a melting pot!

	- This is jewish propaganda, pushed originally by Israel Zangwill among 
	others:
	> The real American has not yet arrived. He is only in the Crucible, I 
	tell you - he will be the fusion of all races, perhaps the coming superman.
	
	- Do Africans deserve the right to their own nations in Africa? Do whites?
	
	- Do Jews deserve their state of Israel? Do whites?
	

- The Great Reset is a conspiracy theory!

	- [Link them the world economic forum's own website](https://www.weforum.org/great-reset)


- The Bilderberg group does not exist!

	- So you're saying elites do *NOT* have a tendency to band together to make 
	more profits?
	
	- You're right, mafias and criminal organizations don't exist. (roll your 
	eyes for effect)


- The US is a democracy!

	- The US is an oligarchy, statistically speaking, a law or bill having 
	popular support is a negative indicator that it will pass and having support
	of the richest 25% is an indicator that it will.
	[link](https://talkingpointsmemo.com/livewire/princeton-experts-say-us-no-longer-democracy)
	
	- The US, even if it wasn't an oligarchy, isn't a democracy but a republic.
	It is so in name only as the constitution is only a piece of paper at this
	point.
	

- Democracy is the best system we have so far!
	
	- Hitler had a 95% approval rating and was democratically elected (hide 
	your smile while saying this if you're a fan of Adolf.)
	
	- In democracies most people vote. Most people are idiots. Most people get
	their news from the TV. Most media is controlled by 6 corporation sin the 
	US. 
	Therefore, democracies are plutocracies or oligarchies. (furthermore those
	6 US corporations have their critical decision positions occupied mostly
	by jews, therefore it can be asserted that democracies are judeocracies).


- The Chinese are behind it all! Biden is a Chinese agent!

	- The US is priming boomers for war with China and Russia in case they need
	to launch another offensive. 
	
	- American-Chinese animus is motivated by facebook boomer memes.
	
	- There is no single piece of evidence that proves Biden is a Chinese
	asset other than Fox news repeating it. News outlets repeating a story is
	evidence of its falsehood, not truthfulness.


- Fuck boomers / Don't Trust Anyone Over 30

	- Jack Weinberg uttered the phrase – which became one of the most 
	memorable expressions of the turbulent 1960s era – during the height of the 
	Free Speech Movement at UC Berkeley. The Free Speech Movement was a 
	struggle by students over the right to engage in political speech on campus, 
	which helped to catalyze broader political activism on campuses around the 
	country over student rights, civil rights and the Vietnam War.

	- Do not let propaganda separate and divide you from your elders. They may
	be a bit foolish at times, but disconnecting from them will only leave you
	more vulnerable to the clutches of evil.


- The Greek were homosexuals / pederasts / adulterers.

	- Outright false. Because it undermines the roots of European legacy, white
	haters love repeating that the Greek were disgusting adulterers and child
	rapists. This is false, we have historic legal documents that state that
	adultery was punished with death, same for pederasty.
	Homosexuality was likewise disgusting to the Greeks, as much as it is today
	to any healthy man.
	
	- [This post explains it best](https://www.eurocanadians.ca/2018/04/the-myth-of-homosexuality-in-ancient-greece.html)
	
	- A book was written on the topic: Homosexuality in Ancient Greece: The Myth is Collapsing.
	

- Projection: you are mentally ill!

	- Left wingers correlate to mental illness. [study](https://www.researchgate.net/publication/339541044_Mental_illness_and_the_left)
	
	- Of the last 10 people who yelled at you about politics, how many were 
	right-wingers?



#### Covid


- Covid is the deadliest illness ever!
	
	- It isn't. The death figures are inflated but even then.


- Covid vaccines are good: will help us return to normal
	
	- they haven't, even now. People are being asked to take more boosters, we 
	have no reason to believe those will work either.
	
	- (ineffective) they aren't vaccines, they are mRNA treatment!


- Covid vaccines are good: Trust the science!

	- I do not trust pharmaceuticals. You shouldn't either.
	
	- [Pfizer paid the largest fraud settlement ever.](https://www.justice.gov/opa/pr/justice-department-announces-largest-health-care-fraud-settlement-its-history)


#### So where do these taking points come from?

The oligarchy has a few main mechanisms of indoctrination, listed here by their
effectiveness in my opinion:

- The media: US media is run by 6 corporations, headed mostly by jews.

- Public education: Again, ran mostly by Bolshevik jews in the US.

- Celebrities public outreach programs: ran for particular agendas.

!["Hello, I'm Tom Hanks,The U.S. government has lost its credibility so it's borrowing some of mine." - Tom Hanks in the simpsons movie](https://uploads.dailydot.com/2022/01/joe-biden-tom-hanks-the-simpsons.jpg?auto=compress%2Cformat&ixlib=php-3.3.0)