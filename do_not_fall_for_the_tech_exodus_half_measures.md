tags: tech, addiction


# The tech exodus: do not fall for the tech exodus trap

![satire about the federal government using lies to pass control bills](/static/img/how-would-you-like-this-wrapped-fed.png)

*So you've taken an interest in politics, and as a result you've been banned?
You may be tempted to join Gab, and while I won't tell you not to, it is an
incomplete solution!*

This is advice on how to not be booted off of the internet. If you're at the 
point where you worry about OPSEC on the internet, this is not the tutorial for 
you.

 - Have a look at [privacytools.io](https://www.privacytools.io/) to understand 
how to protect your privacy online.

---

You've been banned because you fell on the wrong side of the spectrum of 
acceptable opinion that unaccountable oligarchs deem appropriate speech; 
Welcome to political correctness!

There are five solutions to this problem:

1. Leave the internet, go outside.
2. Do not speak about politics
3. Speak about whatever the overlords consider acceptable.
4. Leave for another platform that promises freedom of speech.
5. Leave for a platform where you cannot be banned by design.

So, which would *you* pick?

---


## The short version

1. Is doable for a day or two and you should attempt it.
2. Read below.
3. This is admitting defeat, we don't do that because we are excellent people.
4. Is a trap: *those who do not learn from history are doomed to repeat it*,
this is because you can **still** be banned, after all, if someone promises they
won't ban you, it just means they can but won't, for now.
5. Is the right choice, and I'll explain how and why.


### Decentralized platforms

(vs centralized ones that tell you they'll be good)

**Decentralized means no single server or authority.**

The advantage of such platforms is that the only form of banning available is
to be ostracized: To be on everyone's blacklist simultaneously, but whatever the
case, you cannot be banned.

Centralized platforms, ran by people you like, who promise you they will protect
your free speech, are, historically speaking, just as likely to ban you as any
other.

After all, Google's slogan was *Do not be evil*. History tells us all of the 
companies and websites fighting for free speech can turn against you, and they
very likely will.


#### Not only about decentralization and freedom of speech.

Software should also:

- Respect your privacy: being free to speak but data-mined is a Pyrrhic victory.

- Be [**free** as in freedom](https://www.gnu.org/philosophy/free-sw.en.html), 
or at least open source, so that we can check for ourselves we aren't being 
spied, or can be censored.


---


## Which Platforms are decentralized? Which respect privacy and/or freedom?

[AlternativeTo](alternativeto.net) is your best friend for finding replacement
software and social media!


### Youtube: video sharing.

- **PeerTube** is a *federated* video sharing platform that uses some of 
Mastodon's technology to function. It is the most resilient in principle, and
you can self-host!

- **BitChute** is useful in theory but not in practice. It is theoretically 
based on P2P functionality, but the website is centralized and will remain so
until people are able to co-host. Furthermore, they block videos based on the
law, so Europeans sometimes have a hard time viewing videos due to antisemitism
or hate-speech laws.

- **Odysee** is a more practical implementation of what BitChute tried to do, 
by, in my opinion, more technically minded individuals. It is a front-end (or
client) to the LBRY protocol, a blockchain media sharing protocol.

- **LBRY** through the desktop or Android app. The LBRY protocol can be browsed
directly through a desktop app instead of using Odysee.

Odysee is a centralized front-end to LBRY and thus, has the ability to censor
who sees videos.

I recommend hosting PeerTube yourself if you have the technical 
ability.


### Twitter: short format social media.

- **Mastodon** is free, open source, federated. Nothing more to say, it is all 
that I would ask.

- **Gab**, Gab is centralized and ran by American right-wingers.
Their domain registrar, epik.com suffered a major leak for stupid reasons.
We can assume Gab won't betray you, but they may accidentally leak all your
information instead, which is worse.

While Gab may not be ideal, you can use it if you want to reach right-wingers 
and other similar public.

I do not know of examples where they have banned National Socialists, but 
frustrated leftists HAVE left the site out of their own volition.


### Facebook: long format social media.

- **Host your own website with wordpress** and realize how little people care
about your mundane life. Never use facebook or talk about your private life
unless you are asked by someone who cares.

- **Minds** checks out every box, open source, decentralized, built for free 
speech by technologically competent people.

- **Diaspora** similar to above, but federated. Sounds jewish so I suspect it is
shit.


### Google email.

- **Protonmail** is an acceptable option, this is as long as you understand the
golden principle that email is not an encrypted protocol by default. So simply
put, do not send anything secret by email.

- **Tutanota** same as above but paid.

- **Cockmail** or cock.li (You read that right). A dude hosting an email server.
Requires invites to join and an email client.

- **Self-hosting**. Self-hosting an email server used to be difficult. Now there
are countless tutorials on how to do this, however, many agree that it simply 
isn't worth the hassle, as you shouldn't be using email for secure 
communication.


### Discord, communications.

- **Tox** is FOSS, and distributed, but the VoIP features leave a bit to be 
desired. Has a choice of clients. Normies may have a bit of problems setting it
up or complain that they have to use other services, so you may need to get 
better friends.

- **Mumble  for voice chat**. Open source, can self host (server is named 
murmur). Leaves a lot to be desired, seems to only use Open Source as a selling
point rather than respecting users as a baseline.

- **Element** uses the matrix protocol. A bit more heavy to self-host a server
than Tox. It is the most normie-friendly alternative from the UI-UX perspective.
However, it features *diversity* on its ads, which indicate it may just be 
posturing in regards to respecting its users and having principles.


### Google Chrome, Firefox, Browsers.

- **Librewolf**. Free, Open Source, Firefox engine, but without the Firefox 
bullshit. Install uBlock Origin and you're golden. Can be further enhanced to
further protect privacy, but this is outside of scope for this article.

- **NOT: Firefox**. This bears saying just in case you were misinformed: 
Firefox does not care about your privacy, and they are Google's paid bitch so
that congress doesn't have a case of anti-trust against google and breaks up
Chrome.
This is the only reason Firefox as it is exists today. They are data-brokers as
much as google are.

- **Maybe: Ungoogled chromium**. *Not chromium* as it still includes google 
tracking. The reason you shouldn't use the google engine for browsing is because
handing google monopoly over how the internet is as good as not fighting against
big tech at all. But hey, maybe you need the V8 engine, so at the very least, 
use a version with google's obsessive data gathering removed. Again install 
uBlock Origin at the very least.

- **Brave**, an acceptable choice if you don't want to put in any effort, or are
asked for a quick recommendation on what to use.


#### Why not DuckDuckGo?

DuckDuckGo has used google tracking cookies on their users.

Remember this lesson: those who promise they will respect you have no actual
obligation to do so, whether that be your privacy or freedom of speech.


## A note on Strategy

In a perfect world, everybody would be using decentralized or federated content
hosting.

But this is not the perfect world.

If you want to connect with right-wing Americans, use Gab.

If you want to get your friends off of Whatsapp, but can't do more than getting
them to use Telegram, that's fine as well.

> Done is better than perfect. (Mark Zuckerberg)