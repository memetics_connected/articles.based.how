tags: tech, addiction

# Avoid excessive social media usage

![stop sign over a smartphone](https://i.postimg.cc/rpyHRd9S/no-phone.jpg)

*So you think there may be something wrong with your internet usage?
Welcome! Read on.*

This is beginner's advice. If you're at the point where you worry about OPSEC 
on the internet, this is not the tutorial for you.

 - Have a look at [privacytools.io](https://www.privacytools.io/) to understand 
how to protect your privacy online.

---

The internet is a net negative influence on most people's lives as of this 
moment.

This article will explain the why and how this is, and offer advice in how to 
remediate this problem. You are welcome to skip to the **advice** section at
any point.


## Social media was made to keep you using it, and this hurts you.

Most teenagers will develop social or psychological problems at some point or 
another, caused directly by the way they are used by social media. *You do not 
use social media, social media uses you.*

"Social media" makes us asocial. It provides a faux sense of socialization, this 
is harmful.

Some people theorize that this is an unwitting consequence of the design social 
media has.
This is false. Social media was created with misanthropic goals in mind. Below 
are the objectives websites such as facebook, twitter, instagram, tiktok and 
other strive for **by design**:

- Social media is designed to keep you using it: Auto-play, infinite scroll, 
algorithms decide what you're most likely to consume to keep you on the site.

- Social media and most of the internet mines your data, this is accessible to 
both enemy number 1; the US federal government or equally horrible groups such 
as the Chinese or Israeli governments, and ruthless corporation who want 
nothing more than to get you to buy useless shit you don't need. Most cannot 
help this aggressive sales strategy.


## Smartphones are spying devices

[The Pentagon Ended Online Surveillance Project Same Day Facebook Was Founded.](https://i.postimg.cc/XN28ZMDF/9.jpg)

Just recently, google has removed the ability *not* to send recordings that your
phone takes of you to their servers.

If you listen to what experts in the field are saying, such as Julian Assange, 
Chelsea Manning and especially Edward Snowden, they will tell you outright that
your smartphones are used to spy on you, to datamine you.


## There are worse things happening in silicon valley

Tech giant google has influenced elections both in the US and abroad.

If they have this capacity, what stops them from influencing individual 
decisions?

Indeed they already are. Google runs political propaganda right on their image 
search results.

Amazon analyzes the best way to aggressively push a sale and manipulates you 
into buying useless shit you don't need.

Twitter, being a much more political site, has both banned and undermined the 
reach that any persona-non-grata has on the platform.

**Etc.**


## Social Media isn't the only big bad, but it is definitely the biggest

You likely know of a few people who cannot help endlessly scrolling through 
their smartphones, zombified by the constant stream of bullshit going on.

Every generation has theirs: Facebook for boomers, TikTok for Zoomers, or 
Instagram for Millennials.

Another fantastic example of psychological dependence abuse is YouTube.

A series of creators are monetarily incentivized to make ever-more catchy videos 
with attractive titles and thumbnails. Without any incentive not to engage in 
click-bait or other predatory methods. If they do not do so, they will be 
pushed out by the competition.

Youtube also has a terrific feature to keep zombies from waking up: Autoplay; an 
algorithm decides what is most likely to keep you watching the screen and 
automatically feeds you it.

It doesn't help that Silicon Valley leans heavily to the left and it shows in 
their authoritarian treatment of the masses.

If you wish to know more, research "embrace, extend, extinguish" to understand 
just how Microsoft would treat you if you were to threaten their interests. 
Most other tech giants are about as evil or worse.

---


# Advice 

On how to avoid social media and tech in general.


## Identify the problem

If you've read the above section, you're likely aware of how problematic social 
media is, and tech in general.

Tech isolates us, and dehumanizes us. We are social animals meant to interact 
directly with members of our own tribe. Social Media and tech is a direct 
attack to this.


## Take back control:

### Stop using your smartphone for two weeks.

You are most likely addicted to your smart-phone. This is simple to understand:
Anything that releases dopamine can become an addictive. TikTok weaponized this
to keep you watching videos.

So we have to destroy this addiction first and foremost, everything else is 
secondary unless you can control yourself.

You will be tempted to use half-measures to control this urge. 
You will most likely fail if you take this attitude. You need a real first step.


### Reduce vicious tech use in general.

Once you've managed to get a hold of your addiction, you may use your phone 
again, now in better control of it.

Here are some notes:

- Pick a day of the week or the weekend, do not use tech in this day... read,
camp out, engage in a hobby or socialize.

- Do not interact with screens when it is dark out, it messes with your sleep. 
It is in fact 
a very bad habit.

- Uninstall social media. Only and only ever use something if you have a need 
for it. *Everyone else uses it* is not a need.

- Leave your smartphone at home. You don't need it most of the time and it just
sits there tempting you anyways, or worse, recording you.

- Learn about EU GDPR and apply it, remove your old unused social media and 
other accounts from the web.

*[EU GDPR]: General Data Protection Regulation of the European Union.