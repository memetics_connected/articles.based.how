tags: meta


# TODO: articles

A list of articles that would be nice to have on the site.

Submissions can be sent to [submissions@based.how](mailto:submissions@based.how)

- [philosophy] The ancients reading guide.

Where to start, what you should know, recommendations, and quick summaries of 
many books.

- [philosophy] Important definitions for often misunderstood terms.

Such as freedom, responsibility, etc.

- [self improvement] Self improvement: Man as a beast becoming a god.

- [biology] Nofap, pornography.

How to heal negative sexuality though asceticism, appendix on shilling and 
cointelpro (meming to absurdity) of nofap, and the abusive nature of the porn
industry and mindgeek.

- [biology, sex] Healthy sexual relationships and the art of loving.

How to obtain positive sexual wholesomeness and be a better partner.

- [biology] Sub-speciation in the Homo Sapiens.

With relevant statistics, and the *true* history of the understanding of races.

- [finances] Basics of cryptocurrency.

How monero can destroy the fed, and why McAfee was killed for revealing this.

- [finance, history] The economical enslavement of the world through 
centralized banking, a summary.

Why usury is evil and how to avoid it. Alternative title: Banking 101.

Important: How to be your own bank and save your investments from inflation.

- [history] An abridged history of the US minus political correctness.

