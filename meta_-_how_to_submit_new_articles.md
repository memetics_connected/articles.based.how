tags: meta

# Submitting articles for based.how

Thank you for taking an interest in contributing to [based.how](https://based.how)

You can do this in two ways:

 - opening a PR for [this repository](https://gitlab.com/memetics_connected/articles.based.how)
 with a new .md file.

 - sending your article to submissions@based.how or 
 submissions.based.how@protonmail.com


## Guidelines on writing articles

The **theme** of all articles is *how to be based, redpilled, or arete*.

All articles should have useful knowledge that acts as a guide for
the purpose of aligning oneself with the universe and its laws.

The *formatting* is basic [markdown](https://www.markdownguide.org/cheat-sheet/)
with a caveat: The first line should always be the following:
```
Tags: aaaaaa, bbbbb, ccccccc
```

This is to say, a list of tags separated by commas, starting with `Tags:`.

The title of the article with which it is submitted *should* be the same as the
first `# title` of the article and be fairly descriptive. the filename format 
is the following: `underscore_separated_title.md`. The exception are meta 
articles, meaning articles about the webpage. Do not include symbols in the 
title when submitting new articles.

If you are publishing with someone's permission, the article should start with
a section noting the source, and the name of the file should start with 
`mirror_-_underscore_separated_title.md`.


## Linking to news articles

All links to news articles should be archived. This is for several reasons:

- Links suffer link-rot, news publishers have often moved article urls.

- In a similar vein, news publishers have made changes or corrections.

- Denial of click revenue.

- Denial of tracking, cookies, and other potential malware.

- Denial of gate-keeping, check out this example:

[The Telegraph article, cannot read without subscription](http://www.telegraph.co.uk/news/health/news/9426205/Cannabis-smoking-permanently-lowers-IQ.html),
[Same but archived, can read freely](https://archive.fo/444XV).

- Hiding origin information, as visiting other places may link those to where 
you visited from, here.

Linking to other places is fine, but shouldn't be done un-necessarily.


## Particulars of formatting

Basic [markdown syntax](https://www.markdownguide.org/basic-syntax/) is used.

However, there are some extra plugins that may be useful: 
[sane Lists](https://python-markdown.github.io/extensions/sane_lists/), 
[abbreviations](https://python-markdown.github.io/extensions/abbreviations/), 
[attribute Lists](https://python-markdown.github.io/extensions/attr_list/), 
[definition Lists](https://python-markdown.github.io/extensions/definition_lists/), 
[fenced Code Blocks](https://python-markdown.github.io/extensions/fenced_code_blocks/), and
[tables](https://python-markdown.github.io/extensions/tables/).


### Other things to keep in mind

Articles can include references to external images, and link external sources.

Articles should be relatively short and poignant. Not Zoomer attention span 
short, but short nonetheless.

Write about what you know, and what has worked.

Please be patient when submitting an article, it can take a while for the 
reviewers to approve or deny articles.

There is no reason to accommodate non-meme-speakers, but try to strike a balance
between sounding like a parody of the latest anony*moose* messages, and a real 
person.


---

# Test markdown document

This document is here to provide examples for all markdown features when writing
an article for this website.

It was sourced from [markdown-it demo](https://markdown-it.github.io/).

Use it as a reference or cheat-sheet. 

Note: Some features displayed in the original do not appear here, this is 
because they have not been enabled.

---

__Advertisement :)__

- __[pica](https://nodeca.github.io/pica/demo/)__ - high quality and fast image
  resize in browser.
- __[babelfish](https://github.com/nodeca/babelfish/)__ - developer friendly
  i18n with plurals support and easy syntax.

You will like those projects!

---

# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading

```
# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading
```


## Horizontal Rules

___

---

***

```
___

---

***
```


## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~

```
**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~
```


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.

```
> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.
```


## Lists

Unordered

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

```
+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!
```

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

```
1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`
```

Start numbering with offset:

57. foo
1. bar

```
57. foo
1. bar
```


## Code

Inline `code`

```
Inline `code`
```

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


```
Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code
```


Block code "fences"

```
Sample text here...
```

Surround your code with 3 backticks (`)


## Tables

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

```
| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |
```

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

```
| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |
```


## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

```
[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")
```


### Definition lists

Term 1

:   Definition 1
with lazy continuation.

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

```
Term 1

:   Definition 1
with lazy continuation.

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.
```


### Abbreviations

This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

*[HTML]: Hyper Text Markup Language

```
This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

*[HTML]: Hyper Text Markup Language
```


## Images

![Minion](https://octodex.github.com/images/minion.png)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"

```
![Minion](https://octodex.github.com/images/minion.png)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"
```
